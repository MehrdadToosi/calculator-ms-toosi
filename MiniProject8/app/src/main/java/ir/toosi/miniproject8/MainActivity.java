package ir.toosi.miniproject8;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


    public class MainActivity extends Activity {


        TextView tv1;
        Button btn1;

        Button btn2;
        Button btn3;

        Button btn4;
        Button btn5;
        Button btn6;
        Button btn7;
        Button btn8;
        Button btn9;
        Button btn0;
        Button btnPlus;
        Button btnminu;
        Button btnEqual;
        Button btnzarb;
        Button btntag;
        Double num1 = 0.0;
        Double num2 = 0.0;
        Double num3 = 0.0;
        String num4 ;
        Button btnas;
        private int op;
        Button btnc;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            tv1 = (TextView) findViewById(R.id.tv1);
            btn0 = (Button) findViewById(R.id.btn0);
            btn1 = (Button) findViewById(R.id.btn1);
            btn2 = (Button) findViewById(R.id.btn2);
            btn3 = (Button) findViewById(R.id.btn3);
            btn4 = (Button) findViewById(R.id.btn4);
            btn5 = (Button) findViewById(R.id.btn5);
            btn6 = (Button) findViewById(R.id.btn6);
            btn7 = (Button) findViewById(R.id.btn7);
            btn8 = (Button) findViewById(R.id.btn8);
            btn9 = (Button) findViewById(R.id.btn9);
            btnPlus = (Button) findViewById(R.id.btnPlus);
            btnEqual = (Button) findViewById(R.id.btnEqual);
            btnminu = (Button) findViewById(R.id.btnminu);
            btnzarb = (Button) findViewById(R.id.btnzarb);
            btntag = (Button) findViewById(R.id.btntag);
            btnc=(Button)findViewById(R.id.btnc);
            btnas=(Button)findViewById(R.id.btnas);



            btn0.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("0");

                }
            });
            btn1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("1");

                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("2");

                }
            });
            btn3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("3");

                }
            });
            btn4.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("4");

                }
            });
            btn5.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("5");

                }
            });
            btn6.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("6");

                }
            });
            btn7.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("7");

                }
            });
            btn8.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("8");

                }
            });
            btn9.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.append("9");

                }
            });
            btnc.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    tv1.setText("");
                    num1 = 0.0;
                    num2 = 0.0;

                }
            });

            btnas.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                   tv1.append(".");


                }
            });

            btnEqual.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Double result = 0.0;

                    num2 = Double.parseDouble(tv1.getText().toString());

                        switch (op) {
                        case 1:
                            result = num1 + num2;
                            break;
                        case 2:
                            result = num1 - num2;
                            break;
                        case 3:
                            result = num1 / num2;
                            break;
                        case 4:
                            result = num1 * num2;
                            break;
                        default:
                            break;
                    }
                    tv1.setText(result + "");

                }
            });

            btnPlus.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    num1 = Double.parseDouble(tv1.getText().toString());
                    op = 1;
                    tv1.setText("");
                }
            });

            btnminu.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    num1 = Double.parseDouble(tv1.getText().toString());
                    op = 2;
                    tv1.setText("");
                }
            });

            btntag.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    num1 = Double.parseDouble(tv1.getText().toString());
                    op = 3;
                    tv1.setText("");
                }
            });

            btnzarb.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    num1 = Double.parseDouble(tv1.getText().toString());
                    op = 4;
                    tv1.setText("");
                }
            });

        }
    }



